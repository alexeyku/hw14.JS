// b.keydown == m+
// n.keydown == m-
// m.keydown == mrc
let calc = document.getElementById('box');
let output = document.getElementById('output');
let valueTest = /[0-9]/;
function Constr(){
	let b = '';
	let result = '';
	let sign = '';
	let memory = '';
	let mrcCounter = 0;
	this.setMemory = function (prop){
		memory += +prop
	}
	this.getMemory = function(prop){
		return memory
	}

	this.setMrc = function(prop){
		mrcCounter += prop
	}
	this.getMrc = function(){
		return mrcCounter
	}

	this.setResult = function(prop){
		result += prop
	}
	this.setFinalResult = function(prop){
		result = prop
	}
	this.getResult = function(){
		return result
	}

	this.setSign = function(prop){
		sign = prop
	}

	this.getSign = function(){
		return sign
	}
	this.setSecondVal= function(prop) {
		b += prop
	}
	this.setFinalSecondVal= function(prop) {
		b = prop
	}

	this.getSecondVal = function(){
		return b
	}
	this.returnRes = function(){
		if(this.getSign() == "+"){
			this.setFinalResult(+this.getResult() + +this.getSecondVal()) 
		}
		if(this.getSign() == "-"){
			this.setFinalResult(+this.getResult() - +this.getSecondVal()) 
		}
		if(this.getSign() == "*" && this.getSecondVal()!=''){
			this.setFinalResult(+this.getResult() * +this.getSecondVal()) 
		}
		if(this.getSign() == "/" && this.getSecondVal()!=''){
			if(this.getSecondVal() == '0'){
				this.setFinalResult('На ноль делить нельзя!')
				this.setFinalSecondVal('')
				this.setSign('')
			}
			else{
				this.setFinalResult(+this.getResult() /	 +this.getSecondVal()) 
			}
		}
		this.setFinalSecondVal('')
		this.getResult()	
	}
}

let obj = new Constr();

calc.onclick = function(e) {
	if(valueTest.test(e.target.value) == true || e.target.value == '.'){
		output.value = e.target.value
		if(obj.getSign() == ''){
			if(e.target.value == '.'){
				if(obj.getResult().indexOf('.') == -1){
					obj.setResult(e.target.value);
				}
			}
			else{
				obj.setResult(e.target.value);
			}
			output.value = obj.getResult()	
		}

		else if(obj.getSign() == '='){
			obj.setFinalResult('') 
			obj.setSign('')
			obj.setResult(e.target.value)
		}
		else{
			if(e.target.value == '.'){
				if(obj.getSecondVal().indexOf('.') == -1){
					obj.setSecondVal(e.target.value);
				}
			}
			else{
				obj.setSecondVal(e.target.value);
			}
			output.value = obj.getSecondVal()
		}
	}
	if(e.target.value == '+'){
		obj.returnRes()
		output.value = obj.getResult()
		obj.setSign("+");
	}
	if(e.target.value == '-'){
		obj.returnRes()
		output.value = obj.getResult()
		obj.setSign("-");
	}
	if(e.target.value == '*'){
		obj.returnRes()
		output.value = obj.getResult()
		obj.setSign("*");
	}
	if(e.target.value == '/'){
		obj.returnRes()
		output.value = obj.getResult()
		obj.setSign("/");
	}

	if(e.target.value == '='){
		obj.returnRes()
		output.value = obj.getResult()
		obj.setFinalSecondVal('')
		obj.setSign('=')
	}
	if(e.target.value == 'C'){
		output.value = 0;
		obj.setFinalResult('');
		obj.setFinalSecondVal('')
		obj.setSign('')
	}

	if(e.target.value == 'm+'){
		obj.setMemory(+obj.getMemory() + +output.value)
	}
	if(e.target.value == 'm-'){
		obj.setMemory(+obj.getMemory() - +output.value)
	}
	if(e.target.value == 'mrc'){
		obj.setMrc(1)
		if(obj.getMrc() == 1 && obj.getMemory() !=''){
			output.value = obj.getMemory()
		}
		else{
			obj.setMemory(0);
		}
	}

	if(obj.getMemory() != ''){
		memoryLetter.style.zIndex = 1
	}
	if(obj.getMemory() == ''){
		memoryLetter.style.zIndex = 0
	}
}

document.onkeypress = function(e){
	if(valueTest.test(e.key) == true || e.key == '.'){
		output.value = e.key
		if(obj.getSign() == ''){
			if(e.key == '.'){
				if(obj.getResult().indexOf('.') == -1){
					obj.setResult(e.key);
				}
			}
			else{
				obj.setResult(e.key);
			}
			output.value = obj.getResult()	
		}

		else if(obj.getSign() == '='){
			obj.setFinalResult('') 
			obj.setSign('')
			obj.setResult(e.key)
		}
		else{
			if(e.key == '.'){
				if(obj.getSecondVal().indexOf('.') == -1){
					obj.setSecondVal(e.key);
				}
			}
			else{
				obj.setSecondVal(e.key);
			}
			output.value = obj.getSecondVal()
		}
	}
  if(e.key == '+'){
    obj.returnRes()
    output.value = obj.getResult()
    obj.setSign("+");
  }
  if(e.key == '-'){
    obj.returnRes()
    output.value = obj.getResult()
    obj.setSign("-");
  }
  if(e.key == '*'){
    obj.returnRes()
    output.value = obj.getResult()
    obj.setSign("*");
  }
  if(e.key == '/'){
    obj.returnRes()
    output.value = obj.getResult()
    obj.setSign("/");
  }

	if(e.key == '=' || e.keyCode == 13){
    	obj.returnRes()
    	output.value = obj.getResult()
    	obj.setFinalSecondVal('')
    	obj.setSign('=')
	}
	if(e.key == 'c'){
    	output.value = 0;
    	obj.setFinalResult('');
    	obj.setFinalSecondVal('')
    	obj.setSign('')
	}


	if(e.key == 'b'){
		obj.setMemory(+obj.getMemory() + +output.value)
	}
	if(e.key == 'n'){
		 obj.setMemory(+obj.getMemory() - +output.value)
	}
	if(e.key == 'm'){
		obj.setMrc(1)
    	if(obj.getMrc() == 1 && obj.getMemory() !=''){
      		output.value = obj.getMemory()
    	}
    	else{
      		obj.setMemory(0);
    	}
	}
	
	if(obj.getMemory() != ''){
    memoryLetter.style.zIndex = 1
  }
  if(obj.getMemory() == ''){
    memoryLetter.style.zIndex = 0
  }
}
